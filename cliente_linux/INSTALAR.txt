 - Dependencias:
   sudo apt-get install libcurl4-openssl-dev libproxy1-plugin-webkit libnl-cli-3-200
 - Instalación:
  sudo dpkg -i nsgclient64.deb
 - Uso:
  - Ejecutar NSGClient
  - Entrar a la web: https://vpn.remoto.tgt.net.ar/vpn/index.html !IMPORTANTE! Hay que simular que usamos Internet Explorer, usar alguna extencion que te cambie el User Agent (Ejemplo: User-Agent Switch en chrome)

Si les da este error:
NSGClient: /usr/lib/x86_64-linux-gnu/libcurl.so.4: version `CURL_OPENSSL_3' not found

Hagan esto (depende de la version del linux):
sudo apt-get remove --auto-remove libcurl4-openssl-dev
sudo apt-get install libcurl3