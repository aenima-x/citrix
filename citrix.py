#!/usr/bin/env python
import os
import getpass
import subprocess
import sys
from urllib.parse import urljoin
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from requests import Session, request
import yaml
import psutil

CITRIX_URL = 'https://vpn.remoto.tgt.net.ar/'
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko'
CONFIG_FILE = '~/.citrix.yml'


class CitrixConnector:
    def __init__(self, proxy=None):
        self.session = Session()
        self.session.headers.update({'User-Agent': USER_AGENT})
        if proxy:
            self.session.proxies = proxy
            self.session.verify = False
        index_url = urljoin(CITRIX_URL, 'vpn/index.html')
        self.session.get(index_url)


    def login(self, user, passwd, domain):
        passwd1 = '7KmFtMBann7C65v0GDawsfvwa3WraF'
        data = {'login': user, 'passwd': passwd, 'passwd1': passwd1, 'domainvalue': domain.upper()}
        login_url = urljoin(CITRIX_URL, 'cgi/login')
        self.session.headers.update({'Referer': urljoin(CITRIX_URL, '/vpn/index.html')})
        self.session.cookies.set('domainvalue', domain.upper())
        r = self.session.post(login_url, data=data)
        sms_url = urljoin(CITRIX_URL, 'cgi/dlge')
        try:
            sms_code = input('SMS Code: ')
        except Exception as e:
            print('Cancel')
            return None
        else:
            data = {'response': sms_code}
            self.session.headers.update({'Referer': login_url})
            r = self.session.post(sms_url, data=data)
            return self.session.cookies.get('NSC_AAAC')


    def start_vpn(self, ns_aaac):
        nsloc = urljoin(CITRIX_URL, 'vpns/f_ndisagent.html')
        client_url = f'http://localhost:3148/svc?NSC_AAAC={ns_aaac}&nsloc={nsloc}&nsversion=10.1.132.8&nstrace=DEBUG&nsvip=255.255.255.255&nstdi=ON'
        print('Starting VPN...')
        try:
            r = request('get', client_url)
        except:
            pass

    def check_for_client(self):
        p_list = psutil.process_iter()
        f_list = [] 
        PROCESS_NAME = None
        command = None
        if sys.platform == 'darwin':
            PROCESS_NAME = "NetScaler Gateway"
            command = ["open", "-a", PROCESS_NAME]
        elif sys.platform == 'linux':
            PROCESS_NAME = "NSGClient"
            command = [PROCESS_NAME]
        if PROCESS_NAME:
            p_found = False
            for p in p_list:
                try:
                    if p.name() == PROCESS_NAME:
                        break
                except:
                    pass
            if p_found:
                print("Client Running [OK]")
            else:
                print("Client Not Running...starting it")
                subprocess.Popen(command)    
 

    def connect(self):
        self.check_for_client()
        print('Starting Citrix Connection...')
        if os.path.isfile(os.path.expanduser(CONFIG_FILE)):
            print("Loading config...")
            with open(os.path.expanduser(CONFIG_FILE)) as f:
                config = yaml.load(f, Loader=yaml.FullLoader)
                user = config['user']
                password = config['password']
                domain = config['domain']
        else:
            user = input('Username: ')
            password = getpass.getpass('Password: ')
            domain = input('Domain: ')
        ns_aaac = self.login(user, password, domain)
        if ns_aaac:
            self.start_vpn(ns_aaac)


def main():
    citrix_connector = CitrixConnector()
    citrix_connector.connect()


if __name__ == '__main__':
    main()
