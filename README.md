### Requerimientos

* Python 3
* Cliente de Citrix (VPN)
   

### Instalacion
* Instalar Python3
* Instalar dependencias: ```pip3 install -r requrements.txt```

### Ejecutar

```
./citrix.py
```

### Credenciales

Por default les pide las credenciales cada vez que se conecta, pueden cargarlas en un archivo para que no sa las pida.

```
cp citrix_example.yml $HOME/.citrix.yml
```

En ese archivo completen sus credenciales.
```
user: user
password: password
domain: domain
```

#### Notas:
En mac y linux verifica que este corriendo el cliente, sino lo levanta.

En windows no, pero ya es un servicio por lo cual deberia estar siempre arriba

Miren que no verifica que las credenciales esten correctas (va para la proxima version),
por lo cual si estan mal nunca les va a llegar el SMS. 

Si las cambian recuerden actualizar el archivo.
